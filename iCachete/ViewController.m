//
//  ViewController.m
//  iCachete
//
//  Created by Vicente de Miguel on 3/2/15.
//  Copyright (c) 2015 Nicatec. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;
#import "CafPlayer.h"

@interface ViewController ()
//Que vistas voy a necesitar
@property (strong,nonatomic) IBOutlet UILabel *info;
@property (strong,nonatomic) IBOutlet UIImageView *image;

@property (strong,nonatomic) NSArray *culoArray;
@property (strong,nonatomic) NSArray *japonArray;
@property (strong,nonatomic) NSArray *espaldaArray;
@property (strong,nonatomic) NSArray *espejoArray;
@property (strong,nonatomic) NSArray *afinaPunteriaArray;

@property (strong, nonatomic) NSMutableArray *sonidosArray;
@property (strong,nonatomic) CafPlayer *player;


//defino una macro para sacar un numero aleatorio entero entre 0 y a
#define aleatorio(a) arc4random_uniform(a)

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.player = [CafPlayer cafPlayer];

    
    // Do any additional setup after loading the view, typically from a nib.
    
    //genero las vistas y las situo en la vista principal, respetando los 20 puntos de la barra del reloj
    self.info=[[UILabel alloc]initWithFrame:CGRectMake(0.0, 20.0, self.view.frame.size.width, 44.0)];
    self.image=[[UIImageView alloc] initWithFrame:CGRectMake(0.0, 64.0, self.view.frame.size.width, self.view.frame.size.height-44)];
    
    //alineo el texto en el centro e inicializo
    self.info.textAlignment=NSTextAlignmentCenter;
    self.info.text=@"Ay mi amol, dame un cachetito!";
    self.image.image=[UIImage imageNamed:@"icachete.jpg"];
    
    //creo el gesto de tap
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapTheImage:)];

    
    //añado las vistas y el gesto a la vista principal
    [self.view addSubview:self.image];
    [self.view addSubview:self.info];
    [self.view addGestureRecognizer:tap];

    
    //voy a usar unos arrays para sacar textos
    self.japonArray=[NSArray arrayWithObjects:@"Premio gordo mi amol",@"Que punteria tienes",@"Ponme como Japon",@"10.000$", nil];
    self.culoArray=[NSArray arrayWithObjects:@"Dame fuelte que me gusta",@"Asi, asi!!",@"iCachete for ever",@"Cachetito cachetito",@"Veo que te gustan mis mollitas", nil];
    self.espaldaArray=[NSArray arrayWithObjects:@"Si mi amol, tambien me gusta",@"No esta mal, mejor mas abajo",@"Mejor en el iCachete", nil];
    self.espejoArray=[NSArray arrayWithObjects:@"No mi amol, es un espejo",@"Prefiero que me cachetees a mi",@"Mi amol, cuidado no lo rompas", nil];
    self.afinaPunteriaArray=[NSArray arrayWithObjects:@"Asi no mi amol",@"Apunta mejol",@"Todo lo haces asi de mal?", nil];
    
    
    self.sonidosArray = [NSMutableArray arrayWithCapacity:3];
    NSBundle *b = [NSBundle mainBundle];
    for (NSString *name in @[@"15917", @"15918", @"15919"]) {
        
        [self.sonidosArray addObject:[b URLForResource:name
                                         withExtension:@"mp3"]];
    }
   
    }

#pragma mark - TapGesture
-(void)tapTheImage:(UITapGestureRecognizer *)sender {
    //defin unos cuadrados para saber donde han tocado
    //El CGRectMake crea un frame posicion x,y y ancho y alto
    //x=0,y=0 es la esquina superior izquierda
    
    CGRect culo=CGRectMake(24, 513, 153, 123);
    CGRect japon=CGRectMake(100, 590, 20, 20);
    CGRect espalda=CGRectMake(24, 210, 153, 303);
    CGRect espejo=CGRectMake(200, 65, self.view.frame.size.width-200, self.view.frame.size.height);
    //Compruebo que el gesto ha sido reconocido sin problemas
    if ( sender.state == UIGestureRecognizerStateRecognized ) {
        //obtengo el punto donde han tocado, punto no lleva * xq es un struct, no un objeto
        CGPoint punto = [sender locationInView:self.view];
        //NSLog(@"Tocan x=%f y=%f",punto.x,punto.y);
        
        //Ahora compruebo donde han tocado, utilizo una funcion que comprueba su un cuadrado contiene un punto, los cuandrados son los que defini al principio
        if ( CGRectContainsPoint(japon, punto)) {
            /*de los arrays que creo al principio con los textos que puse, genero uno aleatorio entre 0 y el numero de elementos que tengo, de ahi se saca uno aleatorio y es el que pongo en el texto
            */
            self.info.text=[self.japonArray objectAtIndex:aleatorio((unsigned int)[self.japonArray count])];
        } else if (CGRectContainsPoint(culo, punto)) {
            self.info.text=[self.culoArray objectAtIndex:aleatorio((unsigned int)[self.culoArray count])];
        } else if ( CGRectContainsPoint(espalda, punto)) {
            self.info.text=[self.espaldaArray objectAtIndex:aleatorio((unsigned int)[self.espaldaArray count])];
        } else if ( CGRectContainsPoint(espejo, punto)) {
            self.info.text=[self.espejoArray objectAtIndex:aleatorio((unsigned int)[self.espejoArray count])];
        } else {
            self.info.text=[self.afinaPunteriaArray objectAtIndex:aleatorio((unsigned int)[self.afinaPunteriaArray count])];
            
        }
        
        // Sonido
        self.player = [CafPlayer cafPlayer];
        [self.player playFile:self.sonidosArray[1]];
        
        [self.player playFile:[self.sonidosArray objectAtIndex:aleatorio((unsigned int)[self.sonidosArray count])]];
        
        

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"Pues estamos jodidos!");
}

@end
